#lang info
(define collection "cuid")
(define deps '("base"))
(define build-deps '("scribble-lib" "racket-doc" "htdp-doc" "at-exp-lib" "sandbox-lib" "compatibility-doc"))
(define pkg-desc "Cuid: A secure, portable, and sequentially-ordered identifier")
(define version "0.1")
(define pkg-authors '(theodesp))
